package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    int randomIndexChoice = (int)(Math.random())*rpsChoices.size();
    Random rand = new Random();
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
            System.out.printf("Let's play round %d\n", roundCounter);

            String humanChoice = userChoice();
            String computerChoice = rpsChoices.get(rand.nextInt(rpsChoices.size()));

            if (findWinner(humanChoice, computerChoice)) {
                System.out.println ("Human chose " + humanChoice +", computer chose " + computerChoice + ". Human wins!");
                humanScore+=1;
                System.out.println("Score: human " + humanScore +", computer " + computerScore);
            }

            else if (findWinner(computerChoice, humanChoice)) {
                System.out.println("Human chose " + humanChoice +", computer chose " + computerChoice + ". Computer wins!");
                computerScore+=1;
                System.out.println("Score: human " + humanScore +", computer " + computerScore);
            }

            else {
                System.out.println("Human chose " + humanChoice +", computer chose " + computerChoice + ". It's a tie!");
                System.out.println("Score: human " + humanScore +", computer " + computerScore);
            }

            String continueanswer = continueplaying();
            if (continueanswer.equals("n")){
                break;
            }
            else{
                roundCounter+=1;
                continue;
            }
        }
        
        System.out.println("Bye bye :)");
    }



    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    public Boolean findWinner(String choice1, String choice2) {
        if (choice1.equals("paper")){
            return choice2.equals("rock");
        }
        else if (choice1.equals("scissors")) {
            return choice2.equals("paper");
        }
        else {
            return choice2.equals("scissors");
        }


    }

    public Boolean validateInputChoices(String input, List<String> validinputs) {
        input=input.toLowerCase();
        return validinputs.contains(input);
    }

    public String continueplaying() {
        while (true) {
            List<String> continueanswers = Arrays.asList("y","n");
            String humanAnswer = readInput("Do you wish to continue playing? (y/n)?");
            if (validateInputChoices(humanAnswer, continueanswers)){
                return humanAnswer;
            }
            else {
                System.out.printf("I do not understand " + humanAnswer +". Could you try again?\n");
            }
            }

    }

    public String userChoice() {
        while (true) {
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (validateInputChoices(humanChoice, rpsChoices)){
                return humanChoice;
            }
            else{
                System.out.println("I do not understand " + humanChoice + ". Could you try again?");
            }


        }
    }
    }